from urllib.request import urlopen
from bs4 import BeautifulSoup

html = urlopen("http://www.pythonscraping.com/pages/warandpeace.html")
# html = urlopen("http://tverhoney.ru")
bsObj = BeautifulSoup(html, "html.parser")
# bsObj = BeautifulSoup(html)
nameList = bsObj.findAll("span", {"class":"green"})
# nameList = bsObj.findAll("div", {"class":"col-xs-10 about-grid-text"})
# nameList = bsObj.findAll("div", {"class":"service-info"})
# nameList = bsObj.findAll("div", {"class":"service-image"})

for name in nameList:
    print(name.get_text())
    # print(name)

# print(nameList[7].get_text())