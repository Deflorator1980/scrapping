from urllib.request import urlopen
from bs4 import BeautifulSoup

html = urlopen("http://www.pythonscraping.com/pages/warandpeace.html")
bsObj = BeautifulSoup(html, "html.parser")
# nameList = bsObj.findAll("span", {"class":"green"})
# nameList = bsObj.findAll(text='the prince')
# nameList = bsObj.findAll(id='text')
nameList = bsObj.findAll("", {"id":"text"})

print(len(nameList))
print(nameList[0])
# print(nameList[0].get_text())

# for name in nameList:
#     print(name.get_text())