from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup

def getTitle(url):
    try:
        html = urlopen(url)
    except HTTPError as e:
        return None
    try:
        bsObj = BeautifulSoup(html.read())
        title = bsObj.h1
        # title = bsObj
    except AttributeError as e:
        return None
    return title

# title = getTitle("http://www.pythonscraping.com/pages/page1.html")
title = getTitle("http://tverhoney.ru/")
if title == None:
    print("Hui!")
else:
    print(title)